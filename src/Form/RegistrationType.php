<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,
                ['attr'=>['class'=>'form-control']]
            ,['label'=>'nom'])
            ->add('prenom',TextType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('email',EmailType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('password',PasswordType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('passwordConfirm',PasswordType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('adresse',TextType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('telephone',NumberType::class,
                ['attr'=>['class'=>'form-control']])
            ->add('dateNais',\Symfony\Component\Form\Extension\Core\Type\DateType::class,
                ['widget'=>'single_text'],
                ['attr'=>['class'=>'form-control']
                    ],[
                    'label' => false
                ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
