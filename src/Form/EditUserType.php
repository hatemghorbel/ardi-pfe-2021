<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',TextType::class,['attr'=>['class'=>'form-control validate']])
            ->add('roles',ChoiceType::class,[
                'choices' => [
                    'Utilisateur' => 'ROLE_USER',

                    'Administrateur' => 'ROLE_ADMIN'
                ],

                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles',

            ],['attr'=>['class'=>'form-check-input']]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
