<?php

namespace App\Form;

use App\Entity\Convention;
use App\Entity\Partenaire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom',TextType::class,['attr'=>['class'=>'form-control'],
                'label'=>'Nom'])
            ->add('lieu',TextType::class,['attr'=>['class'=>'form-control'],
                'label'=>'Lieu'])
            ->add('dateConv',DateType::class,
                ['widget'=>'single_text','attr'=>['class'=>'form-control'],
                    'label'=>'date convention'])
            ->add('dateDeb',TimeType::class,['widget'=>'single_text','attr'=>['class'=>'form-control']])
            ->add('datefin',TimeType::class,['widget'=>'single_text','attr'=>['class'=>'form-control']])
            ->add('description',TextareaType::class,['attr'=>['class'=>'form-control'],
                'label'=>'description'])
            ->add('partenaires',EntityType::class,
                [
                    'attr'=>['class'=>'form-control'], 'class' => Partenaire::class,
                    'multiple' => true,
                    'label'=>'Partenaire',
                    'expanded' => false,
                    'choice_label'=> function ($partenaire){
                        return $partenaire->getResponsable();
                    }
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convention::class,
        ]);
    }
}
