<?php

namespace App\Form;

use App\Entity\Sponsor;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SponsorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Nom'])
            ->add('modePayement',ChoiceType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'choices'  => [
                    ''=>'',
                    'test' => 'test',
                    'test1' => 'test1',

                ],
            ])
            ->add('montant',NumberType::class,['attr'=>['class'=>'form-control form-control-sm'],
                'label'=>''])
            ->add('matriculeFiscal',NumberType::class,['attr'=>['class'=>'form-control form-control-sm'],
                'label'=>''])
            ->add('email',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>''])
            ->add('tel',NumberType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>''])
            ->add('date',DateType::class,
                ['widget'=>'single_text',
                'attr'=>['class'=>'form-control']
                ],[
                    'label' => false
                ])
            ->add('logo',FileType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'label'=> '',
                'data_class' => null,
                'required' => true

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sponsor::class,
        ]);
    }
}
