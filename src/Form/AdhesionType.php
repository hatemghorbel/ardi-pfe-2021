<?php

namespace App\Form;

use App\Entity\Adherant;
use App\Entity\Adhesion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdhesionType extends AbstractType
{

    private function getConfiguration($label,$placeholder,$options = []){
        return array_merge([
            'label'=>$label,
            'attr'=>[
                'placeholder'=>$placeholder
            ]
        ],$options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adherant',EntityType::class,['class'=>Adherant::class,
                'choice_label'=> function ($adherant){
                    return $adherant->getPrenom()."".$adherant->getNom();
                },
                'attr'=>['class'=>'form-control form-control-sm'],
            ],$this->getConfiguration("adherant","adherant"))
            ->add('datePaiement',DateType::class,['widget'=>'single_text','attr'=>['class'=>'form-control']],$this->getConfiguration("date","date paiement"))
            ->add('anneeAdhesion',DateType::class,['widget'=>'single_text','attr'=>['class'=>'form-control']],$this->getConfiguration("date adhesion","date adhesion"))
            ->add('montant',NumberType::class,['attr'=>['class'=>'form-control']],$this->getConfiguration("Montant","Montant"))
            ->add('typePaiement',ChoiceType::class, [
                'choices'  => [
                    'Especes' => 'Especes',
                    'Cheque' => 'Cheque',

                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adhesion::class,
        ]);
    }
}
