<?php

namespace App\Form;

use App\Entity\Adherant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdherantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Nom'])
            ->add('prenom',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Prenom'])
            ->add('email',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Email'])
            ->add('adresse',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Adresse'])
            ->add('telephone',NumberType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'telephone'])
            ->add('fonction',ChoiceType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'choices'  => [
                    ''=>'',
                    'Admin' => 'Admin',
                    'Enseignant' => 'Enseignant',
                    'Etudiant' => 'Etudiant',
                ],
            ])
            ->add('cin',NumberType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'cin'])
            ->add('photo',FileType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'label'=> '',
                'data_class' => null,
                'required' => true

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adherant::class,
        ]);
    }
}
