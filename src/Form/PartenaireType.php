<?php

namespace App\Form;

use App\Entity\Partenaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartenaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Responsable',TextType::class,['attr'=>['class'=>'form-control'],
                'label'=>'Responsable'])
            ->add('FonctionResponsable',TextType::class,['attr'=>['class'=>'form-control'],
                'label'=>'FonctionResponsable'])
            ->add('Presedent',TextType::class,['attr'=>['class'=>'form-control'],
                'label'=>'Presedent'])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partenaire::class,
        ]);
    }
}
