<?php

namespace App\Form;

use App\Entity\Evenement;
use App\Entity\Sponsor;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomEvent',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>'Nom'])
            ->add('typeEvent',ChoiceType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'choices'  => [

                    'Conférences' => 'Conférences',
                    'Séminaire' => 'Séminaire',
                    'Workshop' => 'Workshop',

                ],
            ])
            ->add('dateEvent',DateType::class,
                ['widget'=>'single_text',
                    'attr'=>['class'=>'form-control']
                ],[
                    'label' => false
                ])
            ->add('datefin',DateType::class,
                ['widget'=>'single_text',
                    'attr'=>['class'=>'form-control']
                ],[
                    'label' => false
                ])
            ->add('lieu',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>''])

            ->add('responsableEvent',TextType::class,
                ['attr'=>['class'=>'form-control form-control-sm'],
                    'label'=>''])
            ->add('logo',FileType::class, [
                'attr'=>['class'=>'form-control form-control-sm'],
                'label'=> '',
                'data_class' => null,
                'required' => true

            ])
            ->add('sponsors',EntityType::class,[
                'attr'=>['class'=>'form-control'], 'class' => Sponsor::class,
                'multiple' => true,
                'label'=>'Partenaire',
                'expanded' => false,
                'choice_label'=> function ($sponsor){
                    return $sponsor->getNom()."".$sponsor->getLogo();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }
}
