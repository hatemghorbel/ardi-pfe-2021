<?php

namespace App\Controller;

use App\Entity\PasswordUpdate;
use App\Entity\User;
use App\Form\PasswordUpdateType;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class CompteController extends AbstractController
{
    /**
     * @Route("/login", name="compte_login")
     */
    public function login(AuthenticationUtils $utils): Response
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        return $this->render('compte/login.html.twig',[
            'hasError'=>$error !==null,
            'username'=> $username
        ]);
    }

    /**
     * @Route("/logout", name="compte_logout")
     */
    public function logout(){

    }

    /**
     * @Route("/register", name="compte_register")
     */
    public function register(Request $request,EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder) : Response{
        $user = new User();

        $form=$this->createForm(RegistrationType::class,$user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash=$encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                "votre compte a bien ete cree"
            );
            return $this->redirectToRoute("compte_login");
        }
        return  $this->render('compte/registration.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/password-update",name="compte_password")
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder,EntityManagerInterface $manager){
        /**@var User $user */
        $user = $this->getUser();
        $passwordUpdate = new PasswordUpdate();

        $form = $this->createForm(PasswordUpdateType::class,$passwordUpdate);

        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            if(!password_verify($passwordUpdate->getOldPassword(),$user->getPassword())) {
                $form->get('oldPassword')->addError(new FormError("le mot de passe que vous avez tapé n'est pas votre mot de passe actuel !"));
            }else{
                $newPassword =$passwordUpdate->getNewPassword();

                $hash = $encoder->encodePassword($user,$newPassword);
                $user->setPassword($hash);

                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "votre mot de passe a bien ete modifier !"
                );
                return $this->redirectToRoute('acceuil');
            }
        }
        return $this->render('compte/password.html.twig',[
            'form'=>$form->createView()
        ]);

    }
}
