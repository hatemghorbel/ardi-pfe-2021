<?php

namespace App\Controller;

use App\Entity\Sponsor;
use App\Form\SponsorType;
use App\Repository\SponsorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/sponsor")
 */
class SponsorController extends AbstractController
{
    /**
     * @Route("/", name="sponsors")
     */
    public function index(SponsorRepository $sponsorRepository): Response
    {
        return $this->render('sponsor/index.html.twig',[
            'sponsors'=>$sponsorRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_sponsor"), methods={"GET","POST"}
     */
    public function new (Request $request):Response
    {
        $sponsor =new Sponsor();
        $form=$this->createForm(SponsorType::class,$sponsor);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $file = $sponsor->getLogo();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $entityManager = $this->getDoctrine()->getManager();
            $sponsor->setLogo($fileName);
            $entityManager->persist($sponsor);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "sponsor ajouter avec succès"
            );
            return $this->redirectToRoute('sponsors');
        }
        return $this->render('sponsor/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }
    /**
     * @Route ("/{id}/edit" ,name="edit_sponsor")
     */
    public function edit(Request $request,$id) :Response{
        $sponsor=$this->getDoctrine()->getRepository(Sponsor::class)->find($id);
        $form =$this->createForm(SponsorType::class,$sponsor);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $form->get('logo')->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $entityManager = $this->getDoctrine()->getManager();
            $sponsor->setLogo($fileName);
            $entityManager->flush();

            $this->addFlash(
                'success',
                "sponsor modifier avec succès"
            );
            return $this->redirectToRoute('sponsors');
        }
        return $this->render('sponsor/edit.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/{id}",name="delete_sponsor"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $sponsor= $this->getDoctrine()->getRepository(Sponsor::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($sponsor);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "sponsor supprimer avec succès"
        );

        return $this->redirectToRoute('sponsors');
    }
}
