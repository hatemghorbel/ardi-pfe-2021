<?php

namespace App\Controller;

use App\Entity\Convention;
use App\Form\ConventionType;
use App\Repository\ConventionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/convention")
 */
class ConventionController extends AbstractController
{
    /**
     * @Route("/", name="conventions")
     */
    public function index(ConventionRepository $conventionRepository): Response
    {
        return $this->render('convention/index.html.twig',[
            'conventions'=>$conventionRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_convention"), methods={"GET","POST"}
     */
    public function new (Request $request):Response
    {
        $convention =new Convention();
        $form=$this->createForm(ConventionType::class,$convention);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($convention);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "convention ajouter avec succès"
            );
            return $this->redirectToRoute('conventions');
        }
        return $this->render('convention/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route ("/{id}/edit" ,name="edit_convention")
     */
    public function edit(Request $request,$id) :Response{
        $convention=$this->getDoctrine()->getRepository(Convention::class)->find($id);
        $form =$this->createForm(ConventionType::class,$convention);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $this->addFlash(
                'success',
                "convention modifier avec succès"
            );
            return $this->redirectToRoute('conventions');
        }
        return $this->render('convention/edit.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/{id}",name="delete_convention"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $adhesion= $this->getDoctrine()->getRepository(Convention::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($adhesion);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "convention supprimer avec succès"
        );

        return $this->redirectToRoute('conventions');
    }

}
