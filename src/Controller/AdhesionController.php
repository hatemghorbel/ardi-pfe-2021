<?php

namespace App\Controller;

use App\Entity\Adhesion;
use App\Form\AdhesionType;
use App\Repository\AdhesionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/adhesion")
 */
class AdhesionController extends AbstractController
{
    /**
     * @Route("/", name="adhesions")
     */
    public function index(AdhesionRepository $adhesionRepository): Response
    {
        return $this->render('adhesion/index.html.twig',
        [
            'adhesions'=>$adhesionRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_adhesion"), methods={"GET","POST"}
     */
    public function new (Request $request):Response
    {
        $adhesion =new Adhesion();
        $form=$this->createForm(AdhesionType::class,$adhesion);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($adhesion);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "adhesion ajouter avec succès"
            );
            return $this->redirectToRoute('adhesions');
        }
        return $this->render('adhesion/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route ("/{id}/edit" ,name="edit_adhesion")
     */
    public function edit(Request $request,$id) :Response{
        $adhesion=$this->getDoctrine()->getRepository(Adhesion::class)->find($id);
        $form =$this->createForm(AdhesionType::class,$adhesion);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $this->addFlash(
                'success',
                "adhesion modifier avec succès"
            );
            return $this->redirectToRoute('adhesions');
        }
        return $this->render('adhesion/edit.html.twig',[
            'form'=>$form->createView()
    ]);
    }

    /**
     * @Route ("/{id}",name="delete_adhesion"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $adhesion= $this->getDoctrine()->getRepository(Adhesion::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($adhesion);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "adhesion supprimer avec succès"
        );

        return $this->redirectToRoute('adhesions');
    }

}
