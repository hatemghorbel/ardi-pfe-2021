<?php

namespace App\Controller;

use App\Entity\Adherant;
use App\Form\AdherantType;
use App\Repository\AdherantRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/adherant")
 */
class AdherantController extends AbstractController
{
    /**
     * @Route("/", name="adherants")
     */
    public function index(AdherantRepository $adherantRepository): Response
    {
        return $this->render('adherant/index.html.twig',[
            'adherants'=>$adherantRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_adherant"), methods={"GET","POST"}
     */
    public function new (Request $request):Response {
        $adherant = new Adherant();
        $form = $this->createForm(AdherantType::class,$adherant);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $adherant->getPhoto();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            $entityManager = $this->getDoctrine()->getManager();
            $adherant->setPhoto($fileName);
            $entityManager->persist($adherant);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "Adherant ajouter avec succès"
            );
            return $this->redirectToRoute('adherants');
        }
        return $this->render('adherant/newadherant.html.twig',[
           'form'=> $form->createView()
        ]);
    }

    /**
     * @Route ("/{id}/edit" ,name="edit_adherant")
     */
    public function edit(Request $request,$id) :Response{

        $adherant= $this->getDoctrine()->getRepository(Adherant::class)->find($id);
        $form = $this->createForm(AdherantType::class,$adherant);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $form->get('photo')->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            $entityManager = $this->getDoctrine()->getManager();
            $adherant->setPhoto($fileName);
            $entityManager->flush();

            $this->addFlash(
                'success',
                "Adherant modifier avec succès"
            );

            return $this->redirectToRoute('adherants');
        }
        return $this->render('adherant/edit.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/{id}",name="delete_adherant"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $adherant= $this->getDoctrine()->getRepository(Adherant::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($adherant);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "Adherant supprimer avec succès"
        );

        return $this->redirectToRoute('adherants');
    }
}
