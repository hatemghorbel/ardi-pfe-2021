<?php

namespace App\Controller;

use App\Entity\Partenaire;
use App\Form\PartenaireType;
use App\Repository\PartenaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/partenaire")
 */
class PartenaireController extends AbstractController
{
    /**
     * @Route("/", name="partenaires")
     */
    public function index(PartenaireRepository $partenaireRepository): Response
    {
        return $this->render('partenaire/index.html.twig',
        [
            'partenaires'=>$partenaireRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_partenaire"), methods={"GET","POST"}
     */
    public function new (Request $request):Response
    {
        $partenaire =new Partenaire();
        $form=$this->createForm(PartenaireType::class,$partenaire);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($partenaire);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "partenaire ajouter avec succès"
            );
            return $this->redirectToRoute('partenaires');
        }
        return $this->render('partenaire/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }
    /**
     * @Route ("/{id}/edit" ,name="edit_partenaire")
     */
    public function edit(Request $request,$id) :Response{
        $partenaire=$this->getDoctrine()->getRepository(Partenaire::class)->find($id);
        $form =$this->createForm(PartenaireType::class,$partenaire);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $this->addFlash(
                'success',
                "partenaire modifier avec succès"
            );
            return $this->redirectToRoute('partenaires');
        }
        return $this->render('partenaire/edit.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/{id}",name="delete_partenaire"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $partenaire= $this->getDoctrine()->getRepository(Partenaire::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($partenaire);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "adhesion supprimer avec succès"
        );

        return $this->redirectToRoute('partenaire');
    }

}
