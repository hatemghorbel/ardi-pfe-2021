<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Form\EvenementType;
use App\Repository\EvenementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/evenement")
 */
class EvenementController extends AbstractController
{
    /**
     * @Route("/", name="evenements")
     */
    public function index(EvenementRepository $evenementRepository): Response
    {
        return $this->render('evenement/index.html.twig',[
            'evenements'=>$evenementRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="new_evenement"), methods={"GET","POST"}
     */
    public function new (Request $request):Response
    {
        $evenement =new Evenement();
        $form=$this->createForm(EvenementType::class,$evenement);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $file = $evenement->getLogo();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $entityManager = $this->getDoctrine()->getManager();
            $evenement->setLogo($fileName);
            $entityManager->persist($evenement);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "evenement ajouter avec succès"
            );
            return $this->redirectToRoute('evenements');
        }
        return $this->render('evenement/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }
    /**
     * @Route ("/{id}/edit" ,name="edit_evenement")
     */
    public function edit(Request $request,$id) :Response{
        $evenement=$this->getDoctrine()->getRepository(Evenement::class)->find($id);
        $form =$this->createForm(EvenementType::class,$evenement);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $form->get('logo')->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('uploads_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $entityManager = $this->getDoctrine()->getManager();
            $evenement->setLogo($fileName);
            $entityManager->flush();

            $this->addFlash(
                'success',
                "evenement modifier avec succès"
            );
            return $this->redirectToRoute('evenements');
        }
        return $this->render('evenement/edit.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/{id}",name="delete_evenement"),methods={"DELETE"}
     */
    public function delete(Request $request, $id){
        $evenement= $this->getDoctrine()->getRepository(Evenement::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($evenement);
        $entityManager->flush();
        $this->addFlash(
            'danger',
            "evenement supprimer avec succès"
        );

        return $this->redirectToRoute('evenements');
    }
}
