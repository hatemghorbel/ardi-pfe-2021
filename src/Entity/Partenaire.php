<?php

namespace App\Entity;

use App\Repository\PartenaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PartenaireRepository::class)
 */
class Partenaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Responsable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $FonctionResponsable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Presedent;



    /**
     * @ORM\ManyToMany(targetEntity=Convention::class, mappedBy="partenaires")
     */
    private $conventions;

    public function __construct()
    {
        $this->conventions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResponsable(): ?string
    {
        return $this->Responsable;
    }

    public function setResponsable(string $Responsable): self
    {
        $this->Responsable = $Responsable;

        return $this;
    }

    public function getFonctionResponsable(): ?string
    {
        return $this->FonctionResponsable;
    }

    public function setFonctionResponsable(string $FonctionResponsable): self
    {
        $this->FonctionResponsable = $FonctionResponsable;

        return $this;
    }

    public function getPresedent(): ?string
    {
        return $this->Presedent;
    }

    public function setPresedent(string $Presedent): self
    {
        $this->Presedent = $Presedent;

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->Responsable;
    }

    /**
     * @return Collection|Convention[]
     */
    public function getConventions(): Collection
    {
        return $this->conventions;
    }

    public function addConvention(Convention $convention): self
    {
        if (!$this->conventions->contains($convention)) {
            $this->conventions[] = $convention;
            $convention->addPartenaire($this);
        }

        return $this;
    }

    public function removeConvention(Convention $convention): self
    {
        if ($this->conventions->removeElement($convention)) {
            $convention->removePartenaire($this);
        }

        return $this;
    }
}
