<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307211538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement ADD sponsor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E12F7FB51 FOREIGN KEY (sponsor_id) REFERENCES sponsor (id)');
        $this->addSql('CREATE INDEX IDX_B26681E12F7FB51 ON evenement (sponsor_id)');
        $this->addSql('ALTER TABLE sponsor DROP FOREIGN KEY FK_818CC9D4FD02F13');
        $this->addSql('DROP INDEX IDX_818CC9D4FD02F13 ON sponsor');
        $this->addSql('ALTER TABLE sponsor DROP evenement_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E12F7FB51');
        $this->addSql('DROP INDEX IDX_B26681E12F7FB51 ON evenement');
        $this->addSql('ALTER TABLE evenement DROP sponsor_id');
        $this->addSql('ALTER TABLE sponsor ADD evenement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sponsor ADD CONSTRAINT FK_818CC9D4FD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id)');
        $this->addSql('CREATE INDEX IDX_818CC9D4FD02F13 ON sponsor (evenement_id)');
    }
}
