<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210305193617 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE convention (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, lieu VARCHAR(255) NOT NULL, date_conv DATE NOT NULL, date_deb TIME NOT NULL, datefin TIME NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE convention_partenaire (convention_id INT NOT NULL, partenaire_id INT NOT NULL, INDEX IDX_D0BD5688A2ACEBCC (convention_id), INDEX IDX_D0BD568898DE13AC (partenaire_id), PRIMARY KEY(convention_id, partenaire_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE convention_partenaire ADD CONSTRAINT FK_D0BD5688A2ACEBCC FOREIGN KEY (convention_id) REFERENCES convention (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE convention_partenaire ADD CONSTRAINT FK_D0BD568898DE13AC FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE partenaire DROP logo');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE convention_partenaire DROP FOREIGN KEY FK_D0BD5688A2ACEBCC');
        $this->addSql('DROP TABLE convention');
        $this->addSql('DROP TABLE convention_partenaire');
        $this->addSql('ALTER TABLE partenaire ADD logo VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
